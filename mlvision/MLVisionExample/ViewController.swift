//
//  Copyright (c) 2018 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit

import Firebase

/// Main view controller class.
@objc(ViewController)
class ViewController: UIViewController, UINavigationControllerDelegate {
    
    lazy var vision = Vision.vision()
    var resultsText = ""
    var path = UIBezierPath()
    var initialLocation = CGPoint.zero
    var finalLocation = CGPoint.zero
    var shapeLayer = CAShapeLayer()
    
    @IBOutlet weak var tfSelectedText: UITextView!
    
    var textButtons: [CustomButton] = []
    var choosenButtons: [CustomButton] = []
    
    /// An image picker for accessing the photo library or camera.
    var imagePicker = UIImagePickerController()
    
    // Image counter.
    var currentImage = 0
    
    // MARK: - IBOutlets
    
    @IBOutlet fileprivate weak var detectorPicker: UIPickerView!
    
    @IBOutlet weak var containerForScrollView: UIView!
    @IBOutlet fileprivate weak var photoCameraButton: UIBarButtonItem!
    @IBOutlet weak var detectButton: UIBarButtonItem!
    
    var imageScrollView: ImageScrollView!
    
    var scale: CGFloat = 2 {
        didSet {
            imageScrollView.imageZoomView.subviews.forEach { (view) in
                var border: CGFloat!
                switch scale {
                case 1...2:
                    border = 2
                case 2...3:
                    border = 1.5
                case 3...4:
                    border = 1
                default:
                    border = 0.5
                }
                if view is CustomButton {
                    view.layer.borderWidth = border
                }
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSelectedText.delegate = self
        setupImageScrollView()
//        imageScrollView.addSubview(annotationOverlayView)
//        NSLayoutConstraint.activate([
//            annotationOverlayView.topAnchor.constraint(equalTo: imageScrollView.topAnchor),
//            annotationOverlayView.leadingAnchor.constraint(equalTo: imageScrollView.leadingAnchor),
//            annotationOverlayView.trailingAnchor.constraint(equalTo: imageScrollView.trailingAnchor),
//            annotationOverlayView.bottomAnchor.constraint(equalTo: imageScrollView.bottomAnchor),
//        ])

        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        detectorPicker.delegate = self
        detectorPicker.dataSource = self
        
        let isCameraAvailable = UIImagePickerController.isCameraDeviceAvailable(.front)
            || UIImagePickerController.isCameraDeviceAvailable(.rear)
        if !isCameraAvailable {
            photoCameraButton.isEnabled = false
        }
        
        let defaultRow = (DetectorPickerRow.rowsCount / 2) - 1
        detectorPicker.selectRow(defaultRow, inComponent: 0, animated: false)
        setupView()
        imageScrollView.set(image: #imageLiteral(resourceName: "image"))
    }
    
    func setupImageScrollView() {
        imageScrollView = ImageScrollView(frame: containerForScrollView.bounds)
        containerForScrollView.addSubview(imageScrollView)
        
//        self.imageScrollView.set(image: UIImage(named: Constants.images[currentImage])!)
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: containerForScrollView.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: containerForScrollView.bottomAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: containerForScrollView.trailingAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: containerForScrollView.leadingAnchor).isActive = true
        imageScrollView.reloadDelegate = self
    }
    
    
    func setupView(){
        self.view.layer.addSublayer(shapeLayer)
        self.shapeLayer.lineWidth = 20
        self.shapeLayer.strokeColor = UIColor.blue.cgColor
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - IBActions
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        tfSelectedText.text = ""
        choosenButtons = []
        imageScrollView.imageZoomView.subviews.forEach { (view) in
            if view is CustomButton {
                view.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    @IBAction func detect(_ sender: Any) {
        guard let image = imageScrollView.imageZoomView?.image else {
            alert(message: "Please select image")
            return
        }
        clearResults()
        let row = detectorPicker.selectedRow(inComponent: 0)
        if let rowIndex = DetectorPickerRow(rawValue: row) {
            switch rowIndex {
            case .detectTextInCloudDense:
                let options = VisionCloudTextRecognizerOptions()
                options.modelType = .dense
                removeDetectionAnnotations()
                imageScrollView.zoomScale = 1
                detectTextInCloud(image: image, options: options)
            default:
                return
            
            }
        } else {
            print("No such item at row \(row) in detector picker.")
        }
    }
    
    @IBAction func openPhotoLibrary(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    @IBAction func openCamera(_ sender: Any) {
        guard
            UIImagePickerController.isCameraDeviceAvailable(.front)
                || UIImagePickerController
                    .isCameraDeviceAvailable(.rear)
            else {
                return
        }
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true)
    }
    
    // MARK: - Private
    
    /// Removes the detection annotations from the annotation overlay view.
    private func removeDetectionAnnotations() {
        imageScrollView.imageZoomView?.subviews.forEach { (view) in
            if view is CustomButton {
                view.removeFromSuperview()
            }
        }
    }
    
    /// Clears the results text view and removes any frames that are visible.
    private func clearResults() {
        tfSelectedText.text = ""
        textButtons = []
        choosenButtons = []
        removeDetectionAnnotations()
        self.resultsText = ""
    }
    
    private func showResults() {
        let resultsAlertController = UIAlertController(
            title: "Detection Results",
            message: nil,
            preferredStyle: .actionSheet
        )
        resultsAlertController.addAction(
            UIAlertAction(title: "OK", style: .destructive) { _ in
                resultsAlertController.dismiss(animated: true, completion: nil)
            }
        )
        resultsAlertController.message = resultsText
        resultsAlertController.popoverPresentationController?.barButtonItem = detectButton
        resultsAlertController.popoverPresentationController?.sourceView = self.view
        present(resultsAlertController, animated: true, completion: nil)
        print(resultsText)
    }
    
    /// Updates the image view with a scaled version of the given image.
    private func updateImageView(with image: UIImage) {
        self.imageScrollView.set(image: image)
    }
    
    private func transformMatrix() -> CGAffineTransform {
        guard let image = imageScrollView.imageZoomView.image else { return CGAffineTransform() }
        let imageViewWidth = imageScrollView.imageZoomView.frame.size.width
        let imageViewHeight = imageScrollView.imageZoomView.frame.size.height
        let imageWidth = image.size.width
        let imageHeight = image.size.height

        let imageViewAspectRatio = imageViewWidth / imageViewHeight
        let imageAspectRatio = imageWidth / imageHeight
        let scale = (imageViewAspectRatio > imageAspectRatio)
            ? imageViewHeight / imageHeight : imageViewWidth / imageWidth

        // Image view's `contentMode` is `scaleAspectFit`, which scales the image to fit the size of the
        // image view by maintaining the aspect ratio. Multiple by `scale` to get image's original size.
        let scaledImageWidth = imageWidth * scale
        let scaledImageHeight = imageHeight * scale
        let xValue = (imageViewWidth - scaledImageWidth) / CGFloat(2.0)
        let yValue = (imageViewHeight - scaledImageHeight) / CGFloat(2.0)

        var transform = CGAffineTransform.identity.translatedBy(x: xValue, y: yValue)
        transform = transform.scaledBy(x: scale, y: scale)
        return transform
    }
    
    private func pointFrom(_ visionPoint: VisionPoint) -> CGPoint {
        return CGPoint(x: CGFloat(visionPoint.x.floatValue), y: CGFloat(visionPoint.y.floatValue))
    }
    
    private func process(_ visionImage: VisionImage, with textRecognizer: VisionTextRecognizer?) {
        ProgressManager.show()
        textRecognizer?.process(visionImage) { text, error in
            ProgressManager.success()
            guard error == nil, let text = text else {
                let errorString = error?.localizedDescription ?? Constants.detectionNoResultsMessage
                self.resultsText = "Text recognizer failed with error: \(errorString)"
                self.showResults()
                return
            }
            // Blocks.
            for block in text.blocks {
                let transformedRect = block.frame.applying(self.transformMatrix())
                
                // Lines.
                for line in block.lines {
                    let transformedRect = line.frame.applying(self.transformMatrix())

                    // Elements.
                    for element in line.elements {
                        let transformedRect = element.frame.applying(self.transformMatrix())
                        let button = CustomButton(frame: transformedRect)
                        button.isUserInteractionEnabled = false
                        button.layer.borderWidth = 2.0
                        button.layer.borderColor = UIColor.white.cgColor
                        button.detectedText = element.text
                        self.textButtons.append(button)
                        button.setTitle("", for: .normal)
                        //            label.adjustsFontSizeToFitWidth = true
//                        button.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchDown)
                        button.addTarget(self, action: #selector(self.buttonAction(_:)), for: .allEvents)
                        button.alpha = 1
                        self.imageScrollView.imageZoomView.addSubview(button)
                        button.isUserInteractionEnabled = true
                        
                    }
                }
            }
            self.resultsText += "\(text.text)\n"
        }
    }
    
    @objc func buttonAction(_ sender: CustomButton!) {
        if !choosenButtons.contains(sender) {
            sender.layer.borderColor = UIColor.blue.cgColor
            choosenButtons.append(sender)
            tfSelectedText.text += " " + sender.detectedText
        }
    }
    
    @objc func buttonActionMove(sender: CustomButton!) {
        if !choosenButtons.contains(sender) {
            sender.layer.borderColor = UIColor.blue.cgColor
            choosenButtons.append(sender)
            tfSelectedText.text += " " + sender.detectedText
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let location = touches.first?.location(in: self.view){
            initialLocation = location
        }
    }
    
}

extension ViewController: ReloadDelegate {
    func removeAllSubview() {
       
    }
    
    func reloadButtons(scale: CGFloat) {
        self.scale = scale
    }
}

extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return DetectorPickerRow.componentsCount
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return DetectorPickerRow.rowsCount
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(
        _ pickerView: UIPickerView,
        titleForRow row: Int,
        forComponent component: Int
    ) -> String? {
        return DetectorPickerRow(rawValue: row)?.description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        clearResults()
    }
}

// MARK: - UIImagePickerControllerDelegate

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        clearResults()
        if let pickedImage
            = info[
                convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]
                as? UIImage
        {
            updateImageView(with: pickedImage)
        }
        dismiss(animated: true)
    }
}


/// Extension of ViewController for On-Device and Cloud detection.
extension ViewController {
    
    func alert(message: String, title: String = "", handlerOk: ((UIAlertAction) -> Void)? = nil) {
           DispatchQueue.main.async {
               let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
               let okAction = UIAlertAction(title: "Ok", style: .default, handler: handlerOk)
               alertController.addAction(okAction)
               self.present(alertController, animated: true, completion: nil)
           }
       }
    // MARK: - Vision Cloud Detection
    
    /// Detects text on the specified image and draws a frame around the recognized text using the
    /// Cloud text recognizer.
    ///
    /// - Parameter image: The image.
    func detectTextInCloud(image: UIImage?, options: VisionCloudTextRecognizerOptions? = nil) {
        guard let image = image else { return }
        
        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = UIUtilities.visionImageOrientation(from: image.imageOrientation)
        
        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: image)
        visionImage.metadata = imageMetadata
        
        // [START init_text_cloud]
        var cloudTextRecognizer: VisionTextRecognizer?
        var modelTypeString = Constants.sparseTextModelName
        if let options = options {
            modelTypeString = (options.modelType == .dense)
                ? Constants.denseTextModelName : modelTypeString
            cloudTextRecognizer = vision.cloudTextRecognizer(options: options)
        } else {
            cloudTextRecognizer = vision.cloudTextRecognizer()
        }
        // [END init_text_cloud]
        
        self.resultsText += "Running Cloud Text Recognition (\(modelTypeString) model)...\n"
        process(visionImage, with: cloudTextRecognizer)
    }
}

// MARK: - Enums

private enum DetectorPickerRow: Int {
    case detectTextInCloudSparse = 0
    
    case
    detectTextInCloudDense,
    detectDocumentTextInCloud,
    detectImageLabelsInCloud,
    detectLandmarkInCloud
    
    static let rowsCount = 5
    static let componentsCount = 1
    
    public var description: String {
        switch self {
        case .detectTextInCloudSparse:
            return "Text in Cloud (Sparse)"
        case .detectTextInCloudDense:
            return "Text in Cloud (Dense)"
        case .detectDocumentTextInCloud:
            return "Document Text in Cloud"
        case .detectImageLabelsInCloud:
            return "Image Labeling in Cloud"
        case .detectLandmarkInCloud:
            return "Landmarks in Cloud"
        }
    }
}

private enum Constants {
    static let images = [
        "image_has_text.jpg"
    ]
    
    static let detectionNoResultsMessage = "No results returned."
    static let sparseTextModelName = "Sparse"
    static let denseTextModelName = "Dense"
    
    static let labelConfidenceThreshold: Float = 0.75
    static let smallDotRadius: CGFloat = 5.0
    static let largeDotRadius: CGFloat = 10.0
    static let lineColor = UIColor.yellow.cgColor
    static let fillColor = UIColor.clear.cgColor
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(
    _ input: [UIImagePickerController.InfoKey: Any]
) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (key.rawValue, value) })
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey)
    -> String {
    return input.rawValue
}

extension ViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
}
